import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Room {

    public static void firstRoomExplore(boolean goingToSecondRoom, Scanner playerInput, Character Belbo, ArrayList<Item> itemsCarried) {
        int firstRoom;
        while (!goingToSecondRoom) {
            System.out.println("Drzwi do pomieszczenia otworzyły się bez przeszkód, pokój wyglądał na schludny i zadbany");
            System.out.println("Nic szczególnego nie przyciągneło Twojej uwagi z wyjątkiem tajemniczych drzwi.");
            System.out.println("1. Kim jest moja postać?");
            System.out.println("2. Jakie przedmioty posiadam?");
            System.out.println("3. Jaka jest moja misja?");
            System.out.println("4. Chcę wejść do pomieszczenia za tajemniczymi drzwiami.");
            firstRoom = playerInput.nextInt();
            switch (firstRoom) {
                case 1:
                    System.out.println(Belbo.getCharDescription());
                    System.out.println();
                    break;
                case 2:
                    Item.itemList(itemsCarried);
                    break;
                case 3:
                    Quest.whatIsMyQuest();
                    break;
                case 4:
                    System.out.println("Czy na pewno chcesz przejsc do kolejnego pomieszczenia ? Napisz tak lub nie");
                    String doYouWantToGo = playerInput.next().toLowerCase();
                    if (doYouWantToGo.equals("tak")) {
                        goingToSecondRoom = true;
                    } else
                        break;
            }
        }
    }

    public static void secondRoomExplore(boolean goingToThridRoom, Scanner playerInput, ArrayList<Item> itemsCarried, Item magicAmulet, Item keyToNextRoom) {
        while (!goingToThridRoom) {
            System.out.println("1. Podejdź do drzwi i spróbuj je otworzyć.");
            System.out.println("2. Przyjrzyj się biblioteczce.");
            System.out.println("3. Podejdź do szkieleta.");
            System.out.println("4. Sprawdź ekwipunek.");
            int chooseYourNextMove = playerInput.nextInt();

            switch (chooseYourNextMove) {
                case 1:
                    if (itemsCarried.contains(magicAmulet) && itemsCarried.contains(keyToNextRoom)) {
                        goingToThridRoom = true;
                        System.out.println("Udało Ci się otworzyc drzwi, a gdy przekroczyłeś próg amulet zajaśniał i zniknął.");
                        Item.itemUse(itemsCarried, magicAmulet);
                        Item.itemUse(itemsCarried, keyToNextRoom);
                    } else if (itemsCarried.contains(keyToNextRoom) && !itemsCarried.contains(magicAmulet)) {
                        System.out.println("Udało Ci się otworzyć drzwi, ale magiczna bariera uniemożliwa przejście dalej.");
                    } else if (!itemsCarried.contains(keyToNextRoom) && !itemsCarried.contains(magicAmulet)) {
                        System.out.println("Drzwi ani drgnęły");
                    } else {
                        System.out.println("Drzwi nadal ani drgnęły");
                    }
                    break;
                case 2:
                    System.out.println("Przyglądasz się półką, gdy nagle z regału wypada ksiązka.");
                    System.out.println("Okazuje się, że była ona skrytką w której wnętrzu znajduje się magiczny amulet.");
                    System.out.println("Czy chcesz go ze sobą zabrać? Wpisz 'tak' lub 'nie' jeśli nie chcesz go wziąć");
                    String getTheAmulet = playerInput.next().toLowerCase();
                    if (getTheAmulet.equals("tak")) {
                        if (itemsCarried.contains(magicAmulet)) {
                            System.out.println("W twoim ekwipunku jest już amulet");
                        } else {
                            itemsCarried.add(magicAmulet);
                        }
                    }
                    else {
                        System.out.println("Zostawiasz amulet w książce");
                    }
                    break;
                case 3:
                    System.out.println("Podchodzisz do szkieleta, przy jego głowie na biurku dostrzegasz klucz, który zabierasz ze sobą");
                    if (itemsCarried.contains(keyToNextRoom)) {
                        System.out.println("Klucz już jest Twoim ekwipunku");
                     } else
                        itemsCarried.add(keyToNextRoom);
                    break;
                case 4:
                    Item.itemList(itemsCarried);
            }
        }
    }



    public static void tryingToEnterNextRoom(Scanner playerInput) {
        boolean isPasswordCorrect = false;
        while (!isPasswordCorrect) {
            String passwordToEnter = playerInput.next().toLowerCase();
            if (passwordToEnter.equals("mellon"))
                isPasswordCorrect = true;
            else {
                System.out.println("Drzwi ani drgnęły. Powiedz poprawne hasło.");
            }
        }
    }

    public static void thirdRoomExplore(boolean goingToFinalRoom, Scanner playerInput, ArrayList<Item> itemsCarried, Item doll, Item robeFragment, Item wizardOrb, Item magniHammer) {
        while (!goingToFinalRoom) {
            System.out.println("1. Idz do polnocnej sciany.");
            System.out.println("2. Idz do wschodniej sciany.");
            System.out.println("3. Idz do poludniowej sciany.");
            System.out.println("4. Idz do zachodniej sciany.");
            System.out.println("5. Sproboj przejsc przez magiczną barierę.");
            System.out.println("6. Sprawdź ekwipunek");

            int chooseYourNextMove = playerInput.nextInt();

            switch (chooseYourNextMove) {
                case 1:
                    boolean isFirstRiddleResolved = false;
                    while (!isFirstRiddleResolved) {
                        System.out.println("Na ścianie północnej widzisz malowidło przedstawiające bitwę między orkami a krasnoludami. " +
                                "nad tymże obrazem dostrzegasz natomiast młot, którego głowica wskazuje ścianę zachodnią. " +
                                "Pod malowidłem widzisz natomiast magiczną barierę, która blokuje przejście do kolejnego pomieszczenia.");
                        System.out.println("Gdy zbliżyłes się bliżej ściany, tuż nad drzwiami pojawił się napis, a właściwie zagadka, która brzmi: ");
                        System.out.println("\"Korzeni nie widziało niczyje oko,\n" +
                                "a przecież to coś sięga bardzo wysoko,\n" +
                                "od drzew wybujało wspanialej,\n" +
                                "chociaż nie rośnie wcale.\"");
                        String firstRiddle = playerInput.next().toLowerCase();
                        if (firstRiddle.equals("góra")) {
                            System.out.println("Z ziemi wysunał się podest na którym stała szkatułka, po otworzeniu jej twoim oczą ukazała się lalka, która zabrałeś");
                            itemsCarried.add(doll);
                            isFirstRiddleResolved = true;
                        } else if (itemsCarried.contains(doll)) {
                            System.out.println("Lalka jest już w Twoim ekwipunku");
                        } else {
                            System.out.println("Zła odpowiedź, spróbuj jeszcze raz.");
                        }
                    }
                    break;
                case 2:
                    boolean isSecondRiddleResolved = false;
                    while (!isSecondRiddleResolved) {
                        System.out.println("Na ścianie wschodniej widnieje malowidło przedstawiające elfkę w błękitnej szacie. ");
                        System.out.println("Tym razem malowidło reprezentuję bitwę krasnoludów z goblinami");
                        System.out.println("Po podejściu ponownie pojawią się zagadka");
                        System.out.println("\"Nie ma skrzydeł, a trzepocze, nie ma ust, a mamrocze, \n" +
                                "nie ma nóg, a pląsa, nie ma zębów, a kąsa.\"");
                        String secondRiddle = playerInput.next().toLowerCase();
                        if (secondRiddle.equals("wiatr")) {
                            System.out.println("Z ziemi wysunął się podest na którym stała szkatułka, po jej otworzeniu ukazał Ci się fragment błękitnej szaty, który wziąłeś");
                            itemsCarried.add(robeFragment);
                            isSecondRiddleResolved = true;
                        } else if (itemsCarried.contains(robeFragment)) {
                            System.out.println("Fragment szaty jest już w Twoim ekwipunku");
                        } else {
                            System.out.println("Zła odpowiedź, spróbuj jeszcze raz.");
                        }
                    }
                    break;
                case 3:
                    boolean isThirdRiddleResolved = false;
                    while (!isThirdRiddleResolved) {
                        System.out.println("Trzecia sciana, poludniowa z ktorej przybyles reprezentuje czarodzieja");
                        System.out.println("Tutaj bitwa, która się rozgrywa to konflikt krasnoludów z ogromnymi trollami");
                        System.out.println("Zagadka, która pojawiła się kiedy podszedłeś brzmi");
                        System.out.println("\"Nie można tego zobaczyć ani dotknąć palcami, \n" +
                                "nie można wyczuć węchem ani usłyszeć uszami; \n" +
                                "jest pod górami, jest nad gwiazdami, \n" +
                                "pustej jaskini nie omija, \n" +
                                "po nas zostanie, było przed nami, \n" +
                                "życie gasi, a śmiech zabija.\"");
                        String thridRiddle = playerInput.next().toLowerCase();
                        if (thridRiddle.equals("ciemność")) {
                            System.out.println("Z ziemii wysunał sie podest, na którym stała szkatułka, po jej otwarciu zobaczyłeś magiczną kulę, która zabrałeś ze sobą");
                            itemsCarried.add(wizardOrb);
                            isThirdRiddleResolved = true;
                        } else if (itemsCarried.contains(wizardOrb)) {
                            System.out.println("Magiczna kula jest już w Twoim ekwipunku");
                        } else {
                            System.out.println("Zła odpowiedź, spróbuj jeszcze raz.");
                        }
                    }
                    break;
                case 4:
                    boolean isFourthRiddleResolved = false;
                    while (!isFourthRiddleResolved) {
                        System.out.println("Przy zachdniej ścianie, pojawia się jedno malowidło - samotny krasnolud dzierżący młot walczy z demonem uzbrojonym w płonący miecz");
                        System.out.println("Tuż pod malowidłem znajduje się piedestał nad którym unosi się młot");
                        System.out.println("Na piedestale znajduje się tabliczka z kolejną zagadką");
                        System.out.println("\"Coś, przed czym w świecie nic nie uciecze,\n" +
                                "co gnie żelazo, przegryza miecze,\n" +
                                "pożera ptaki, zwierzęta, ziele,\n" +
                                "najtwardszy kamień na mąkę miele,\n" +
                                "królów nie szczędzi, rozwala mury,\n" +
                                "poniża nawet najwyższe góry.\"");
                        String fourthRiddle = playerInput.next().toLowerCase();
                        if (fourthRiddle.equals("czas")) {
                            System.out.println("Po poprawnie udzielonej odpowiedz, młot powoli opadł na piedestał, chwyciłeś go w dłon, wydawał się dobrze wyważony");
                            itemsCarried.add(magniHammer);
                            isFourthRiddleResolved = true;
                        } else if (itemsCarried.contains(magniHammer)) {
                            System.out.println("Młot spoczywa już w twej dłoni");
                        } else {
                            System.out.println("Zła odpowiedź, spróbuj jeszcze raz.");
                        }
                    }
                    break;

                case 5:
                    System.out.println("Podchodzisz do magicznej bariery");
                    if (itemsCarried.contains(wizardOrb) && itemsCarried.contains(robeFragment) && itemsCarried.contains(doll) && itemsCarried.contains(magniHammer)) {
                        goingToFinalRoom = true;
                        Item.itemUse(itemsCarried, wizardOrb);
                        System.out.println("Po tym jak podchodzisz do bariery, magiczna kula w Twoim ekwipunku zaczyna świecić. I podobnie jak amulet znika w momencie zbliżenia do bariery");
                    } else {
                        System.out.println("Bariera nie znikła i nadal blokuje przejscie");
                    }
                    break;
                case 6:
                    Item.itemList(itemsCarried);
                    break;

            }
        }
    }

    public static void finalRoom(boolean finishStory, Scanner playerInput, ArrayList<Item> itemsCarried, Item doll, Item robeFragment, Random willYouSurvive) {
        while (!finishStory) {
            System.out.println("Co chcesz zrobić?");
            System.out.println("1. Sprawdź swój ekwipunek.");
            System.out.println("2. Spróbuj otworzyć skrzynię.");
            System.out.println("3. Użyj lalki");
            System.out.println("4. Użyj fragmentu szaty");
            System.out.println("5. Użyj młota");
            int chooseYourTarget = playerInput.nextInt();
            switch (chooseYourTarget) {
                case 1:
                    Item.itemList(itemsCarried);
                    break;
                case 2:
                    if (!itemsCarried.contains(doll) && !itemsCarried.contains(robeFragment)) {
                        System.out.println("Skrzynia otwiera się, a jej bogactwo trafia do Twojego ekwipunku.");
                        System.out.println("Cała jaskinia zaczyna się trząść, podejmujesz więc próbę ucieczki.");
                        boolean chanceToLive = willYouSurvive.nextBoolean();
                        if (chanceToLive) {
                            System.out.println("Udało Ci się uciec z jaskini, a bogactwo, które w niej zdobyłeś dało Ci szczęśliwą przyszłość.");
                        } else {
                            System.out.println("Ty i bogactwo, które zdobyłeś zostaliście pogrzebani w tej jaskini.");
                        }
                        finishStory = true;
                    } else {
                        System.out.println("Skrzynia nie drgnęła");
                    }

                    break;
                case 3:
                    System.out.println("Kładziesz lalkę na skrzyni co prowadzi do tego, że lalka się spala");
                    Item.itemUse(itemsCarried, doll);
                    break;
                case 4:
                    System.out.println("Kładziesz fragment szaty na skrzyni,a ten spala się.");
                    Item.itemUse(itemsCarried, robeFragment);
                    break;
                case 5:
                    System.out.println("Niczym Gimli próbujący zniszczyć pierścień próbujesz uderzyć młotem skrzynię, co kończy się tym, że zostajesz odrzucony i uderzasz o ścianę.");
                    break;
            }
        }
    }
}
