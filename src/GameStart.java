import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class GameStart {
    public static void main(String[] args) {
        Scanner playerInput = new Scanner(System.in);
        Item keyToNextRoom = new Item("Zardzewiały klucz", "Stary zardzewiały klucz, wygląda jakby leżał tu od dawna");
        Item magicAmulet = new Item("Magiczny amulet", "Pozwala niszczyć magiczne bariery");
        Item magniHammer = new Item("Młot Magniego", "Starożytny młot, który dzierżył Król Magni IV");
        Item wizardOrb = new Item("Kula czarodzieja", "Kula, który gdy zbliża się do magicznej bariery zaczyna jasnieć w kolorze fioletu");
        Item robeFragment = new Item("Fragment szaty", "Fragmenty szaty o niebieskim kolorze");
        Item doll = new Item("Lalka", "Szmaciana lalka, o dziwo w całkiem dobrym stanie");
        ArrayList<Item> itemsCarried = new ArrayList<>();
        Random willYouSurvive = new Random();
        boolean goingToSecondRoom = false;
        boolean goingToThridRoom = false;
        boolean goingToFinalRoom = false;
        boolean finishStory = false;

        Character Arius = new Character("Arius",
                "poszukiwaczem przygód, który przemierza świat w poszukiwaniu sławy i bogactwa",
                itemsCarried);

        Quest.theBeginningText();
        Room.firstRoomExplore(goingToSecondRoom, playerInput, Arius, itemsCarried);
        Quest.firstRiddleInAdventure();
        Room.tryingToEnterNextRoom(playerInput);
        Quest.secondRoomText();
        Room.secondRoomExplore(goingToThridRoom, playerInput, itemsCarried, magicAmulet, keyToNextRoom);
        Quest.thirdRoomText();
        Room.thirdRoomExplore(goingToFinalRoom, playerInput, itemsCarried, doll, robeFragment, wizardOrb, magniHammer);
        Quest.finalRoomText();
        Room.finalRoom(finishStory, playerInput, itemsCarried, doll, robeFragment, willYouSurvive);
    }



}
