import java.util.ArrayList;

public class Item {
    private String itemName;
    private String itemDescription;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Item(String itemName, String itemDescription) {
        this.itemName = itemName;
        this.itemDescription = itemDescription;
    }

    public static void itemList(ArrayList<Item> items){
        System.out.println("W twoim ekwipunku są nastpępujące przedmioty: ");
        if (items.isEmpty()){
            System.out.println("Twój ekwipunek jest pusty!");
        }
        for (Item item : items) {
            System.out.println(item);
        }
        System.out.println();
    }

    public static void itemUse(ArrayList<Item> item, Item itemUsed){
        item.remove(itemUsed);
    }


    @Override
    public String toString() {
        return "Nazwa przedmiotu: " + getItemName() +" \nOpis przedmiotu: " + getItemDescription();
    }
}
