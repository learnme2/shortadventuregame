import java.util.ArrayList;

public class Character {
    private String charName;
    private String charDescription;
    private ArrayList<Item> itemsCarried;

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    public String getCharDescription() {
        return "Ma na imię " + charName + " i jest " + charDescription;
    }

    public void setCharDescription(String charDescription) {
        this.charDescription = charDescription;
    }

    public ArrayList<Item> getItemsCarried() {
        return itemsCarried;
    }

    public void setItemsCarried(ArrayList<Item> itemsCarried) {
        this.itemsCarried = itemsCarried;
    }

    public Character(String charName, String charDescription, ArrayList<Item> itemsCarried) {
        this.charName = charName;
        this.charDescription = charDescription;
        this.itemsCarried = itemsCarried;
    }
}
