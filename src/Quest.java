public class Quest {

    public static void whatIsMyQuest () {
        System.out.println("Twoje zadanie jest proste, zdobyc skarb i przeżyć resztę swoich dni w bogactwie i szczęsciu, ale czy to Ci się uda ? \n");
    }

    public static void theBeginningText() {
        System.out.println("Wchodzisz do ogromnej jaskini, idąc jej korytarzami światło słońca w coraz mniejszym stopniu oświetla Twoją drogę");
        System.out.println("Z czasem rozpalasz pochodnię gdyż słońce już tutaj nie sięga.");
        System.out.println("Nie wiesz ile czasu upłyneło, aż w końcu docierasz do drzwi prowadzących do pomieszczenia wykutego w skale.");
    }

    public static void firstRiddleInAdventure() {
        System.out.println("Podchodzisz do drzwi na których nie dostrzegasz klamki, rozglądasz się dookoła i odnajdujesz runy, który udaje Ci się odczytać:");
        System.out.println("'Powiedz przyjacielu i wejdz'");
    }

    public static void secondRoomText() {
        System.out.println("Po tym jak poprawnie zgadujesz hasło, otwierają się przed Tobą drzwi do pomieszczenia.");
        System.out.println("Po twoim wejściu światła w tajmniczym pomieszczeniu automatycznie się zapalają. Dostrzegasz, że zapaliły się jakiegoś rodzaju kryształy");
        System.out.println("Rozglądając się po pokoju, dostrzegasz, że po drugiej jego stronie znajdują się drzwi na których o dziwo jest klamka, ale jest tam także zamek.");
        System.out.println("Po twojej prawej stronie znajduję się sporych rozmiarów biblioteka, próbujesz odczytać tytułu książek, ale są w nieznanym Ci języku");
        System.out.println("Zerakjąc po lewo widzisz natomiast spore drewniane biurko przy którym siedzi sam już tylko szkielet, ktorego czaszka opiera się o biurko");
        System.out.println("Co chcesz zrobić?");
    }

    public static void thirdRoomText() {
        System.out.println("Wchodzisz do trzeciego pokoju, światła ponownie zapaliły się same. Uderzyło Cię jak ogromny w porównaniu z poprzednimi jest ten pokój");
        System.out.println("Jest pełen malowidł przedstawiających bitwy, ale i postacie czy bronie. Zaczynasz dostrzegać pewne prawidłowości");
        System.out.println("Każda ściana ma dokładnie jedną postać, zaś w każdej bitwie uczestniczą krasnoludy. ");
    }

    public static void finalRoomText() {
        System.out.println("Wchodzisz do ostatniego pomieszczenia. Na jego środku stoi skrzynia, padania na nią delikatnie światło, jednak w całym pomieszczeniu panuje półmrok.");
        System.out.println("Po podejściu do skrzyni słyszysz tylko jakby wiatr szepczący Ci do ucha 'Złóz ofiarę'");
    }




}
